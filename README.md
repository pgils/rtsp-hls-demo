# RTSP HLS Demo

Embed Gstreamer output on a HTML page served up by Python Flask using HTTP Live Streaming (HLS)

### Purpose
Allowing operators to view "live" video from an NVIDIA Deepstream pipeline running on a Jetson device with following restrictions:
- Supported in "any" web browser (no external programs or plugins required)
- Works on a single port (80) so it works with balenaOS.

### Performance

This demo uses a Gstreamer pipeline with the following plugins:
- rtph264depay: Extracts H264 video from RTP packets
- h264parse: Parses H.264 streams
- hlssink2: HTTP Live Streaming sink/serve

CPU load is very low because no transcoding takes place: the video is extracted from the input stream and written to `.ts` files for HTTP Live Streaming.

HLS video fragment files are stored in `tmpfs` to limit wear of SD/eMMC flash memory.
With the default pipeline config in this demo and a 1080p variable bitrate IP camera input this results in about 15MiB of RAM used for storage.

- Pipeline output can be viewed by adding an encoding stage to the pipeline. **This would obviously increase load although the hardware `nvenc` encoder on Jetson devices can be used.**
- Pipeline input can be viewed by `(nv)tee`-ing the input to the `hlssink2` plugin. *Although the pipeline probably uses a higer resolution/bitrate input than you would want to output/stream from a device*
- Some browsers do not support HLS natively so the `hls.js` library is used.

## Usage
- Set `RTSP_SOURCE` as a device or fleet variable in balena cloud dashboard. (**must be h264 encoded stream**)
- Push the app: `balena-cli push <fleet>`
- Enable public URL for your device and open up `https://<device-id>.balena-devices.com/` in a modern web browser.
