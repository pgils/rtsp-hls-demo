#!/bin/bash

set -eu

rm -rf /hls/*

gst-launch-1.0 rtspsrc location="${RTSP_SOURCE:?}" \
    ! queue ! rtph264depay ! h264parse config-interval=1 \
    ! hlssink2 max-files=10 playlist-length=5 location="/hls/ch%05d.ts" \
playlist-location="/hls/list.m3u8" target-duration=10
